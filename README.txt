CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Features
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module allows you to set the visibility of the information about the
author, for each individual node. Also adds the ability to independently
configure display the author and date of publication.


FEATURES:
---------

The Submitted by specific module:

* Now you can easily set the visibility of the information about the author for
the specific node.
* Separates visibility settings of the author and the date of publication.
* You can set separate visibility settings (author and date) default on the
edit node type page.


REQUIREMENTS
------------

Just the NODE module in Drupal CORE.


INSTALLATION
------------

1. Install the module as normal, see link for instructions.
   Link: https://www.drupal.org/documentation/install/modules-themes/modules-8

3. Go to "Administer" -> "Extend" and enable the Submitted by specific module.


CONFIGURATION
-------------

 * Go to "Structure" -> "Types" and set default values for the necessary types
of node.


MAINTAINERS
-----------

Current maintainers:

 * Vasilii Lukasevich (lukasss) - https://www.drupal.org/user/2764209

Requires - Drupal 8
License - GPL (see LICENSE)
